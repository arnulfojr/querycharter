<?php

namespace QCharts\CoreBundle\Form\Directory;


use Doctrine\Bundle\DoctrineBundle\Registry;
use QCharts\CoreBundle\Form\Transformer\DirectoryTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DirectoryType extends AbstractType
{
    /** @var Registry $doctrine */
    private $doctrine;

    /**
     * DirectoryType constructor.
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('parent', HiddenType::class)
            ->add('Add', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm btn-default-arny'
                ]
            ])
            ;

        $builder->get('parent')->addModelTransformer(new DirectoryTransformer($this->doctrine->getManager()));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class'=>'QCharts\CoreBundle\Entity\Directory',
            'csrf_protection'=>false
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "directory";
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return "directory";
    }

}